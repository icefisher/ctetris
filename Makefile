OBJS=$(addprefix bin/, game.o keyboard.o main.o menu.o renderer.o dancer.o sound.o)
CFLAGS=-g -I/usr/include/SDL2 -ansi
LFLAGS=-lSDL2 -lSDL2_ttf -lSDL2_mixer
PET=petyuntris

all: bin/tetris

bin/tetris: bin bin/assets $(OBJS)
	gcc $(OBJS) -o bin/tetris $(LFLAGS)

bin/%.o: src/%.c
	gcc -c $(CFLAGS) $^ -o $@

bin/assets:
	cp -r src/assets $@

bin:
	mkdir -p bin

clean:
	rm -rf bin/*.o

run: bin/tetris
	bin/tetris

export: bin/tetris
	rm -rf $(PET)
	cp -r bin $(PET)
	rm $(PET)/*.o
	mv $(PET)/tetris $(PET)/$(PET)
	tar cvzf $(PET).tar.gz $(PET)
	rm -rf $(PET)
