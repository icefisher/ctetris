#include <stdio.h>
#include <SDL.h>
#include <assert.h>
#include "renderer.h"
#include "game.h"
#include "keyboard.h"
#include "menu.h"
#include "sound.h"
#include "dancer.h"

/*
static int getfps() {
    static Uint32 frames;
    Uint32 time = SDL_GetTicks();
    frames++;
    return (int) (frames / (time/1000.0));
}
*/
#ifdef _WIN32
    #ifdef _DEBUG
        int wmain()
    #else
        #include <windows.h>
        int WINAPI WinMain( 
        HINSTANCE hInstance, 
        HINSTANCE hPrevInstance, 
        LPSTR lpCmdLine, 
        int nCmdShow)   
#endif
#else
    int main()
#endif
{
    game_t _game;
    game_t *game = &_game;

    r_init();
    s_init();
    game_init(game);
    game->dancer = d_create();
    d_fly(game->dancer);

    while(!game_isquit(game)) {
        kbd_update(game);
        game_update(game);
        d_update(game->dancer);
        r_update(game);
        /* SDL_Delay(100); */
    }

    d_free(game->dancer);
    r_quit();
    s_quit();

    return 0;
}
