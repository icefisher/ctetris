#include <SDL.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>

#include "renderer.h"

#ifdef _WIN32
    #define snprintf sprintf_s
#endif


static TTF_Font* font;
static SDL_Renderer* ren;
static SDL_Texture* brick;
static SDL_Texture* dance;
static SDL_Window* win;

static void setbrickcolor(int color) {
    switch(color) {
    case 0: SDL_SetTextureColorMod(brick, 0x50, 0x50, 0x50); break;
    case 1: SDL_SetTextureColorMod(brick, 0x00, 0xff, 0x00); break;
    case 2: SDL_SetTextureColorMod(brick, 0x00, 0x00, 0xff); break;
    case 3: SDL_SetTextureColorMod(brick, 0xff, 0xff, 0x00); break;
    case 4: SDL_SetTextureColorMod(brick, 0x00, 0xff, 0xff); break;
    case 5: SDL_SetTextureColorMod(brick, 0xff, 0x00, 0xff); break;
    case 6: SDL_SetTextureColorMod(brick, 0xff, 0x7f, 0x00); break;
    case 7: SDL_SetTextureColorMod(brick, 0xff, 0x00, 0x00); break;
    default: SDL_SetTextureColorMod(brick, 0xeb, 0xeb, 0xeb); break;
    }
}

void r_init() {
    SDL_Surface* surfbmp;
    char buf[2048]; 

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)) {
        printf("sdlinit %s\n", SDL_GetError());
        exit(-1);
    }
    if (TTF_Init() == -1) {
        printf("ttfinit %s\n", TTF_GetError());
        exit(-1);
    }

    win = SDL_CreateWindow("Petyuntris", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);
    ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);/* | SDL_RENDERER_PRESENTVSYNC); */

    /*rw = SDL_RWFromConstMem(tile2, sizeof(tile2));
    surfbmp = SDL_LoadBMP_RW(rw, 1);*/
    getasset(buf, "tile2.bmp");
    surfbmp = SDL_LoadBMP(buf);

    brick = SDL_CreateTextureFromSurface(ren, surfbmp);
    SDL_FreeSurface(surfbmp);

    getasset(buf, "dance.bmp");
    surfbmp = SDL_LoadBMP(buf);
    SDL_SetColorKey(surfbmp, SDL_TRUE, SDL_MapRGB(surfbmp->format, 0xff, 0x00, 0xdc));

    dance = SDL_CreateTextureFromSurface(ren, surfbmp);
    SDL_SetTextureAlphaMod(dance, 0xff);
    SDL_FreeSurface(surfbmp);

    getasset(buf, "Capture_it.ttf");
    font = TTF_OpenFont(buf, 32);
}

void r_quit() {
    SDL_DestroyTexture(brick);
    SDL_DestroyTexture(dance);
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_Quit();
}

void r_clear() {
    SDL_SetRenderDrawColor(ren, 0x00, 0x00, 0x00, 0xff);
    SDL_RenderClear(ren);
}

void r_show() {
    SDL_RenderPresent(ren);
}

void r_drawgrid(grid_t *matrix, int isdark) {
    int i,j;
    for(i=0; i<HEIGHT; i++) {
        for(j=0; j<WIDTH; j++) {
            if ((*matrix)[i][j]) {
                if (!isdark)
                    setbrickcolor( (*matrix)[i][j] );
                else
                    setbrickcolor(0);
                r_drawbrick(j, i);
            }
        }
    }
}

void r_drawbrick(int x, int y) {
    SDL_Rect drect = {0, 0, BRICK_SIZE, BRICK_SIZE};
    drect.x = x*BRICK_SIZE + OFFSET_X;
    drect.y = y*BRICK_SIZE + OFFSET_Y;
    SDL_RenderCopy(ren, brick, NULL, &drect);
}

void r_drawborder() {
    int i,j;
    SDL_SetTextureColorMod(brick, 0xeb, 0xeb, 0xeb);
    for(i=0; i<HEIGHT; i++) {
        r_drawbrick(-1, i);
        r_drawbrick(WIDTH, i);
    }
    for(j=0; j<WIDTH; j++) {
        r_drawbrick(j, HEIGHT);
    }
}

void r_drawlabel(char* text, int x, int y, SDL_Color color) {
    SDL_Surface *surf = TTF_RenderText_Blended(font, text, color);
    /*    SDL_Surface *surf = TTF_RenderUTF8_Blended(font, text, color); */
    SDL_Texture *texture = SDL_CreateTextureFromSurface(ren, surf);

    SDL_Rect rect;
    rect.x = x;
    rect.y = y;

    SDL_QueryTexture(texture, NULL, NULL, &rect.w, &rect.h);
    SDL_RenderCopy(ren, texture, NULL, &rect);

    SDL_FreeSurface(surf);
    SDL_DestroyTexture(texture);
}

void r_drawmenu(menu_t *menu) {
    int i, menuoffsetx, menuoffsety;
    SDL_Color enabled = {0xff, 0xff, 0xff, 0xFF};
    SDL_Color selected = {0xff, 0xff, 0x7f, 0xFF};
    SDL_Color disabled = {0x60, 0x60, 0x60, 0xFF};
    SDL_Color color;

    menuoffsetx = OFFSET_X + BRICK_SIZE*3;
    menuoffsety = 10 + 40*3;
    for(i=0; i<NMENU; i++) {
        color = disabled;
        if (menu->active[i])
            color = enabled;
        if (menu->active[i] && menu->current == i)
            color = selected;
        if (menu->current == i) {
            SDL_Rect rect;

            rect.x = menuoffsetx - BRICK_SIZE - 10;
            rect.y = menuoffsety+i*40;
            rect.w = BRICK_SIZE;
            rect.h = BRICK_SIZE;
            setbrickcolor(1);
            SDL_RenderCopy(ren, brick, NULL, &rect);
        }
        r_drawlabel(menu->text[i], menuoffsetx, menuoffsety+i*40, color);
    }

    r_drawlabel("Petyuntris", menuoffsetx - 20, menuoffsety-2*40, enabled);
}


void r_drawnext(figure_t* figure, int isdark) {
    int i,j;
    setbrickcolor(figure->color);
    if (isdark)
        setbrickcolor(0);
    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j]) {
                r_drawbrick(j + WIDTH + 3, i + 5);
            }
        }
    }

}

void r_drawfigure(figure_t* figure) {
    int i,j;
    setbrickcolor(figure->color);
    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j]) {
                r_drawbrick(figure->x + j, figure->y + i);
            }
        }
    }
}

void r_drawinfo(game_t *game) {
    static int x = OFFSET_X + (WIDTH+3)*BRICK_SIZE;
    static int y = 10;
    char buf[1024];
    SDL_Color color = {0xff, 0xff, 0xff, 0xFF};
    SDL_Color disabled = {0x60, 0x60, 0x60, 0xFF};
 
    if (!game_isrun(game)){
        color = disabled;
    }

    r_drawlabel("score", x, y, color);

    snprintf(buf, 1023, "%d", game->score);
    r_drawlabel(buf, x, y + 40, color);

    r_drawlabel("next", x, y + 40*2, color);

    r_drawlabel("level", x, y + 40*5, color);
    snprintf(buf, 1023, "%d", game->level);
    r_drawlabel(buf, x, y + 40*6, color);


}

void r_drawdancer(dancer_t *d) {    
    SDL_RenderCopy(ren, dance, &d->srect, &d->drect);   
}

void r_update(game_t *game) {
    grid_t matrix = {0};  
    int isdark;
/*    int fps = 0;
    SDL_Color clr = {0xff, 0xff, 0xff, 0xff};
    char buf[1024];
*/
    isdark = !game_isrun(game);    
    matrix_add_matrix(&matrix, &game->grid);
    matrix_add_figure(&matrix, &game->figure);

    r_clear();
    r_drawborder();
    r_drawgrid(&matrix, isdark);
    r_drawnext(&game->nextfigure, isdark);
    r_drawinfo(game);
    r_drawdancer(game->dancer);
    if (isdark)
        r_drawmenu(game->menu);

/*
    fps = getfps();
    sprintf(buf, "fps=%d", fps);
    r_drawlabel(buf, 0, 0, clr);
*/
    r_show();
}
