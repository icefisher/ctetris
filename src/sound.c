#include "sound.h"
#include <SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include "renderer.h"

static Mix_Chunk *boom;
static Mix_Chunk *voices[4];
static Mix_Music *music;

void s_init() {
    char buf[1024];

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
        printf("Mix_OpenAudio: %s\n", Mix_GetError());
        exit(-1);        
    }

    getasset(buf, "boom.wav");
    boom = Mix_LoadWAV(buf);
    getasset(buf, "voice_1.wav");
    voices[0] = Mix_LoadWAV(buf);
    getasset(buf, "voice_2.wav");
    voices[1] = Mix_LoadWAV(buf);
    getasset(buf, "voice_3.wav");
    voices[2] = Mix_LoadWAV(buf);
    getasset(buf, "voice_4.wav");
    voices[3] = Mix_LoadWAV(buf);
    getasset(buf, "music.mp3");
    music = Mix_LoadMUS(buf);

    Mix_PlayMusic(music, -1);
}

void s_playboom() {
    Mix_PlayChannel(-1, boom, 0);
}

void s_playvoice() {
    Mix_PlayChannel(-1, voices[rand() % 4], 0);
}

void s_quit() {
    Mix_CloseAudio();
}

void s_setmusic(int enabled) {
    if (enabled) {
        Mix_ResumeMusic();
    } else {
        Mix_PauseMusic();
    }
}