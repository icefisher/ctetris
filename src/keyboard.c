#include "keyboard.h"
#include "game.h"
#include "menu.h"

static int space_pressed = 0;

static void menu_upkey(game_t *game) {
    menu_up();
}

static void menu_downkey(game_t *game) {
    menu_down();
}

static void menu_enterkey(game_t *game) {
    menu_enter(game);
}

static void menu_esckey(game_t *game) {
    menu_esc(game);
}

static void ingame_left(game_t *game) {
    figure_moveleft(&game->figure, &game->grid);
}

static void ingame_right(game_t *game) {
    figure_moveright(&game->figure, &game->grid);
}
static void ingame_down(game_t *game) {
    game->speedup = 1;
}
static void ingame_up(game_t *game) {
    figure_rotate(&game->figure, &game->grid);
}
static void ingame_esc(game_t *game) {
    game_menu(game);
}
static void ingame_space(game_t *game) {   
    if (!space_pressed) {
        game->speedup = -1; /* DIRTY HACK */    
        space_pressed = 1;
    }
}

static void ingame_space_up(game_t *game) {   
    game->speedup = 0;
    space_pressed = 0;
}

static void ingame_down_up(game_t *game) {   
    game->speedup = 0;
}


static void kbd_nothing(game_t *game) {	
}

kbd_t* kbd_menu() {
    static kbd_t kbd;
    int i;
    for(i=0; i<NKEYS; i++){
        kbd.keys[i] = kbd_nothing;
    }

    kbd.keys[KBD_DOWN] = menu_downkey;
    kbd.keys[KBD_UP] = menu_upkey;
    kbd.keys[KBD_ENTER] = menu_enterkey;
    kbd.keys[KBD_ESC] = menu_esckey;

    return &kbd;
}

kbd_t* kbd_ingame() {
    static kbd_t kbd;
    int i;
    for(i=0; i<NKEYS; i++){
        kbd.keys[i] = kbd_nothing;
    }
    kbd.keys[KBD_DOWN] = ingame_down;
    kbd.keys[KBD_UP] = ingame_up;
    kbd.keys[KBD_LEFT] = ingame_left;
    kbd.keys[KBD_RIGHT] = ingame_right;
    kbd.keys[KBD_ESC] = ingame_esc;
    kbd.keys[KBD_SPACE] = ingame_space;
    kbd.keys[KBDUP_DOWN] = ingame_down_up;
    kbd.keys[KBDUP_SPACE] = ingame_space_up;

    return &kbd;
}

void kbd_update(game_t *game) {
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        kbd_t *kbd = game->kbd;
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_LEFT) {
                kbd->keys[KBD_LEFT](game);
            } else if (event.key.keysym.sym == SDLK_RIGHT) {
                kbd->keys[KBD_RIGHT](game);
            } else if (event.key.keysym.sym == SDLK_UP) {
                kbd->keys[KBD_UP](game);
            } else if (event.key.keysym.sym == SDLK_DOWN) {
                kbd->keys[KBD_DOWN](game);
            } else if (event.key.keysym.sym == SDLK_SPACE) {
                kbd->keys[KBD_SPACE](game);
            } else if (event.key.keysym.sym == SDLK_RETURN) {
                kbd->keys[KBD_ENTER](game);
            } else if (event.key.keysym.sym == SDLK_ESCAPE) {
                kbd->keys[KBD_ESC](game);
            }
        } else if (event.type == SDL_KEYUP) {
            if (event.key.keysym.sym == SDLK_DOWN) {
                kbd->keys[KBDUP_DOWN](game);
            } else if (event.key.keysym.sym == SDLK_SPACE) {
                kbd->keys[KBDUP_SPACE](game);
            }
        } else if (event.type == SDL_QUIT) {
            game_quit(game);
        }
    }
}