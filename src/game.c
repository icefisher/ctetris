#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "game.h"
#include "keyboard.h"
#include "menu.h"
#include "sound.h"
#include <stdlib.h>


void matrix_add_figure(grid_t *matrix, figure_t *figure) {
    int i,j;
    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j] != 0 && figure->y+i < HEIGHT && figure->x+j < WIDTH)
                (*matrix)[figure->y+i][figure->x+j] = figure->color;
        }
    }
}


void matrix_print(grid_t *matrix) {
    int i,j;
    for(i=0; i<HEIGHT; i++) {
        for(j=0; j<WIDTH; j++) {
            printf("%d ", (*matrix)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void figure_print(figure_t* figure) {
    int i,j;
    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            printf("%d ", figure->matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}



int figure_width(figure_t* figure) {
    int i,j;
    int nonzero_row = 0;
    for(j=0; j<FOUR; j++) {
        for(i=0; i<FOUR; i++) {
            if (figure->matrix[i][j] != 0) {
                nonzero_row = j;
            }
        }
    }
    return nonzero_row + 1;
}


int figure_height(figure_t* figure) {
    int i,j;
    int nonzero_row = 0;
    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j] != 0) {
                nonzero_row = i;
            }
        }
    }
    return nonzero_row + 1;
}

static int inside_figure(int i, int j) {
    return i >= 0 && i < FOUR && j >= 0 && j < FOUR;
}


void figure_rotate(figure_t* figure, grid_t *grid) {
    int i,j/*,dx,dy*/;
    figure_t buf = {0};

    int height = figure_height(figure);
    int width = figure_width(figure);
    if (figure->y + width > HEIGHT)
        return;
    if (figure->x + height > WIDTH)
        return;

    /* rotating matrix */
    for(i=height-1; i>-1; i--) {
        for(j=FOUR-1; j>-1; j--) {
            buf.matrix[j][height - i - 1] = figure->matrix[i][j];
            /*if (figure->matrix[i][j] == 2) {
            dx = 1 - (height - i - 1);
            dy = 1 - j;
            }*/
        }
    }

    /*	//shifting to center
    for(i=0; i<FOUR; i++) {
    for(j=0; j<FOUR; j++) {
    figure->shftmatrix[i][j] = 0;
    if (inside_figure(i-dy, j-dx))
    figure->shftmatrix[i][j] = buf.matrix[i - dy][j - dx];
    }
    }*/

    /* checking collision */
    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (buf.matrix[i][j] != 0
                && (*grid)[i+figure->y][j+figure->x] != 0)
                return;
        }
    }

    memcpy(figure->matrix, buf.matrix, sizeof(figure->matrix));
}

int figure_isset(figure_t* figure, grid_t *stack) {
    int i,j;
    int nonzero_row = figure_height(figure);
    if (figure->y + nonzero_row >= HEIGHT) {
        matrix_add_figure(stack, figure);
        /* matrix_print(stack); */
        return 1;
    }

    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j] != 0) {
                if (figure->y+i < HEIGHT && (*stack)[figure->y+i][figure->x+j] != 0) {
                    matrix_add_figure(stack, figure);
                    return 2;
                }
            }
        }
    }

    for(i=0; i<FOUR; i++) {
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j] != 0) {
                if (figure->y+i+1 < HEIGHT && (*stack)[figure->y+i+1][figure->x+j] != 0) {
                    matrix_add_figure(stack, figure);
                    /* matrix_print(stack); */
                    return 1;
                }
            }
        }
    }
    return 0;
}

void matrix_clear(grid_t *matrix) {
    memset(*matrix, 0, sizeof(grid_t));
}


void matrix_remove_line(grid_t *matrix, int line) {
    int i,j;
    for(i=line; i>0; i--) {
        for(j=0; j<WIDTH; j++) {
            (*matrix)[i][j] = (*matrix)[i-1][j];
        }
    }
    for(j=0; j<WIDTH; j++) {
        (*matrix)[0][j] = 0;
    }
}

void matrix_add_matrix(grid_t *matrix, grid_t* stack) {
    int i,j;
    for(i=0; i<HEIGHT; i++) {
        for(j=0; j<WIDTH; j++) {
            (*matrix)[i][j] = (*stack)[i][j];
        }
    }
}


int matrix_check_line(grid_t *matrix) {
    int i,j;
    for(i=0; i<HEIGHT; i++) {
        int is_line = 1;
        for(j=0; j<WIDTH; j++) {
            if ((*matrix)[i][j] == 0){
                is_line = 0;
            }
        }
        if (is_line) {
            return i;
        }
    }
    return -1;
}


void figure_next(figure_t* figure){
    static int k = 3;
    figure_t g = {
        1, 1, 0, 0,
        2, 0, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };
    figure_t l = {
        1, 0, 0, 0,
        2, 0, 0, 0,
        1, 1, 0, 0,
        0, 0, 0, 0
    };
    figure_t t = {
        1, 0, 0, 0,
        2, 1, 0, 0,
        1, 0, 0, 0,
        0, 0, 0, 0
    };
    figure_t square = {
        1, 1, 0, 0,
        1, 2, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
    figure_t line = {
        1, 2, 2, 1,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
    figure_t z = {
        1, 1, 0, 0,
        0, 2, 1, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
    figure_t s = {
        0, 1, 1, 0,
        1, 2, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0
    };
    figure_t *figs[7] = {0};
    figs[0] = &g;
    figs[1] = &t;
    figs[2] = &square;
    figs[3] = &line;
    figs[4] = &s;
    figs[5] = &l;
    figs[6] = &z;
    if (k >= 7) {
        k = 0;
    }
    memcpy(figure->matrix, figs[k]->matrix, sizeof(figure->matrix));
    figure->y = 0;
    figure->x = WIDTH/2 - figure_width(figure)/2;
    figure->color = k+1; /* colors from 1 to 7 */
    k++;
}

void figure_moveleft(figure_t* figure, grid_t *grid) {
    int i,j;

    if (figure->x <= 0)
        return;

    for(i=0; i<FOUR; i++){
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j] != 0
                && (*grid)[i+figure->y][j+figure->x-1] != 0)
                return;
        }
    }

    figure->x -= 1;
}

void figure_moveright(figure_t* figure, grid_t *grid) {
    int i,j;

    if (figure->x + figure_width(figure) >= WIDTH)
        return;

    for(i=0; i<FOUR; i++){
        for(j=0; j<FOUR; j++) {
            if (figure->matrix[i][j] != 0
                && (*grid)[i+figure->y][j+figure->x+1] != 0)
                return;
        }
    }

    figure->x += 1;
}

void game_start(game_t *game) {
    game->state = GAME_RUN;
    game->kbd = kbd_ingame();
    menu_disable(0);
    d_score(game->dancer);
    s_setmusic(0);
}

void game_quit(game_t *game) {
    game->state = GAME_QUIT;
    exit(0);
}

void game_menu(game_t *game) {
    game->state = GAME_MENU;
    game->kbd = kbd_menu();
    menu_enable(1);
    d_fly(game->dancer);
    s_setmusic(1);
}

void game_over(game_t *game) {
    game->state = GAME_OVER;
    game->kbd = kbd_menu();
    s_setmusic(1);
    d_fly(game->dancer);
    menu_enable(0);
}

void game_new(game_t *game) {
    game->state = GAME_MENU;
    game->score = 0;
    game->level = 0;
    memset(game->grid, 0, sizeof(game->grid));
    game->speedup = 0;

    figure_next(&game->figure);
    figure_next(&game->nextfigure);

    game->kbd = kbd_menu();
    game->menu = menu_create();
    game_start(game);
}

void game_init(game_t *game) {
    memset(game, 0, sizeof(game_t));
    game->state = GAME_MENU;
    game->figure.x = WIDTH/2;
    game->figure.y = 0;
    game->kbd = kbd_menu();
    game->menu = menu_create();
}

int game_isrun(game_t *game) {
    return game->state == GAME_RUN;
}

int game_isquit(game_t *game) {
    return game->state == GAME_QUIT;
}

int game_ismenu(game_t *game) {
    return game->state == GAME_MENU;
}

void game_update(game_t *game) {
    static Uint32 last_ticks;
    Uint32 ticks;
    figure_t *figure = &game->figure;
    grid_t *grid = &game->grid;
    int status = 999; /* 999 is not returned by figure_isset */
    Uint32 interval = 1000;

    ticks = SDL_GetTicks();
    game->level = (game->score / 10) + 1;
    if (game->level > 10)
        game->level = 10;
    interval = (Uint32)( interval *  ( 1 - game->level / 10.) );

    if (game_isrun(game)) {    
        if (game->speedup == -1) { /* space was pressed */
            while ((status = figure_isset(figure, grid)) == 0) {
                figure->y += 1;
            }
            game->speedup = 0;
        } else if (ticks - last_ticks > interval*(1 - game->speedup*0.8)) {
            last_ticks = ticks;
            status = figure_isset(figure, grid);
        }

        if (status == 0) {
            figure->y += 1;
        } else if (status == 1) {
            int line;
            int hasscore = 0;

            *figure = game->nextfigure;            
            figure_next(&game->nextfigure);

            while( (line = matrix_check_line(grid)) != -1) {
                matrix_remove_line(grid, line);
                game->score++;
                hasscore = 1;
            }

            if (hasscore) {                
                d_score(game->dancer);
            }

            s_playboom();
        } else if (status == 2) {
            game_over(game);
        }
    } else if (game_ismenu(game)) {
        last_ticks = ticks;
    }
}
