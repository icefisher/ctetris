#pragma once

#include "game.h"
#define NMENU 3


typedef struct menu_t {
    char *text[NMENU];
    int active[NMENU];
    int current;
} menu_t;

menu_t* menu_create();

int menu_down();
int menu_up();
int menu_enter(game_t *game);
void menu_esc(game_t *game);
void menu_enable(int cont);
void menu_disable();