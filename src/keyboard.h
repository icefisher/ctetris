#pragma once

typedef struct game_t game_t;

#define NKEYS 9
enum {
    KBD_LEFT = 0,
    KBD_RIGHT,
    KBD_UP,
    KBD_DOWN,
    KBD_SPACE,
    KBD_ESC,
    KBD_ENTER,

    KBDUP_DOWN,
    KBDUP_SPACE,
};

typedef struct kbd_t {
    void (*keys[NKEYS])(game_t *game);
} kbd_t;

kbd_t* kbd_menu();
kbd_t* kbd_ingame();
void kbd_update(game_t *game);