#pragma once

#include "dancer.h"

#define WIDTH 10
#define HEIGHT 15
#define FOUR 4

enum {
    GAME_RUN, GAME_QUIT, GAME_MENU, GAME_OVER
};

typedef struct kbd_t kbd_t;
typedef struct menu_t menu_t;


typedef int grid_t[HEIGHT][WIDTH];

typedef struct figure_t {
    int matrix[FOUR][FOUR];
    int color;
    int x;
    int y;
} figure_t;

typedef struct game_t {
    int state;
    int score;
    int level;
    grid_t grid;
    figure_t figure;
    figure_t nextfigure;
    unsigned int speedup;
    kbd_t *kbd;
    menu_t *menu;
    dancer_t *dancer;
} game_t;

int figure_width(figure_t* figure);
int figure_height(figure_t* figure);
void figure_rotate(figure_t* figure, grid_t* grid);
int figure_isset(figure_t* figure, grid_t *stack);
void figure_next(figure_t* figure);
void figure_moveleft(figure_t* figure, grid_t *grid);
void figure_moveright(figure_t* figure, grid_t *grid);


void matrix_add_figure(grid_t *matrix, figure_t *figure);
void matrix_clear(grid_t *matrix);
void matrix_remove_line(grid_t *matrix, int line);
void matrix_add_matrix(grid_t *matrix, grid_t* stack);
int matrix_check_line(grid_t *matrix);

void game_start(game_t *game);
void game_quit(game_t *game);
void game_new(game_t *game);
void game_menu(game_t *game);
void game_over(game_t *game);

void game_init(game_t *game);
int game_isrun(game_t *game);
int game_isquit(game_t *game);
int game_ismenu(game_t *game);
void game_update(game_t *game);