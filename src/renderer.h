#pragma once

#include "game.h"
#include "menu.h"
#include "dancer.h"

#define OFFSET_X (BRICK_SIZE*4) /*(640/2 - (WIDTH*BRICK_SIZE)/2) */
#define OFFSET_Y 20
#define BRICK_SIZE 24

#ifdef _WIN32
    #define getasset(buf, filename) _snprintf_s((buf), sizeof((buf)),sizeof((buf)), "assets\\"filename);
#else
    #define getasset(buf, filename)                                             \
            do{                                                                 \
                char* path;                                                     \
                path = SDL_GetBasePath();                                       \
                snprintf((buf), sizeof(buf), "%sassets/"filename, path);        \
                SDL_free(path);                                                 \
            } while(0);
#endif

void r_init();
void r_quit();
void r_clear();
void r_show();
void r_drawgrid(grid_t *matrix, int isdark);
void r_drawbrick(int x, int y);
void r_drawborder();
void r_drawmenu(menu_t *menu);
void r_drawnext(figure_t* figure, int isdark);
void r_drawinfo(game_t *game);
void r_drawlabel(char* text, int x, int y, SDL_Color color);
void r_drawdancer(dancer_t *dancer);
void r_update(game_t *game);
