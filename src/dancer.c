#include "dancer.h"
#include "sound.h"
#include <stdlib.h>

static dancer_t __dancer;

dancer_t *d_create() {
    __dancer.x = 400;
    __dancer.y = 300;
    __dancer.state = DANCER_IDLE;
    __dancer.srect.x = 0;
    __dancer.srect.y = 0;
    __dancer.srect.w = DANCE_WIDTH;
    __dancer.srect.h = DANCE_HEIGHT;
    __dancer.drect.x = __dancer.x;
    __dancer.drect.y = __dancer.y;
    __dancer.drect.w = DANCE_WIDTH;
    __dancer.drect.h = DANCE_HEIGHT;

    return &__dancer;
}

void d_free(dancer_t *dancer) {
}

void d_score(dancer_t *dancer) {
    int b[] = {2, 4, 6, 7};

    dancer->ky = b[rand() % 4];
    dancer->kx = 0;

    s_playvoice();
}
void d_fly(dancer_t *dancer) {
    dancer->ky = 8;
    dancer->kx = 0;
}
void d_mad(dancer_t *dancer) {
}

void d_update(dancer_t *dancer) {
    static int lasttick;
    
    dancer->srect.x = dancer->kx*DANCE_WIDTH;
    dancer->srect.y = dancer->ky*DANCE_HEIGHT;

    if (SDL_GetTicks() - lasttick >= 100)
    {
        dancer->kx++;
        lasttick = SDL_GetTicks();
    }

    if (dancer->kx >= 8) {
        dancer->kx = 0;
        if (dancer->ky != 8) { /*not fly*/
            dancer->ky = 0;
            if (rand() % 8 == 0)
                dancer->ky = 1;
        }
    }

}
