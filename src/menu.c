#include "menu.h"
#include "sound.h"

static menu_t menu = {{"new", "resume", "quit"}, {1,0,1}, 0};

menu_t* menu_create() {		
    return &menu;
}

int menu_up() {
    menu.current--;
    if (menu.current < 0){
        menu.current = 0;
        return 0;
    }
    s_playboom();
    return 1;
}
int menu_down() {
    menu.current++;
    if (menu.current >= NMENU) {
        menu.current = NMENU - 1;
        return 0;
    }
    s_playboom();
    return 1;
}

int menu_enter(game_t *game) {
    if (!menu.active[menu.current])
        return 0;
    if (menu.current == 0) {
        game_new(game);
    } else if (menu.current == 1) {
        game_start(game);
    } else if (menu.current == 2) {
        game_quit(game);
    }
    s_playboom();
    return 1;
}

void menu_esc(game_t *game) {
    if (game->menu->active[1]) { /* game was started and menu "resume" is active */
        game_start(game);
        s_playboom();
    }
}

void menu_enable(int cont) {
    int i;
    for(i=0; i<NMENU; i++) {		
        menu.active[i] = 1;
    }
    menu.current = 1;

    if (!cont) {
        menu.active[1] = 0;
        menu.current = 0;
    }
}

void menu_disable() {
    int i;
    for(i=0; i<NMENU; i++) {		
        menu.active[i] = 0;
    }
    menu.current = -1;
}
