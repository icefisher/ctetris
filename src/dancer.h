#pragma once

#include <SDL.h>

typedef struct dancer_t {
    int x;
    int y;
    int kx, ky;
    int state;
    SDL_Rect srect, drect;
} dancer_t;
#define DANCE_WIDTH 110
#define DANCE_HEIGHT 128    

enum {
    DANCER_INTRO, DANCER_SCORE, DANCER_IDLE, DANCER_MADNESS
};

dancer_t *d_create();
void d_free(dancer_t *dancer);
void d_score(dancer_t *dancer);
void d_fly(dancer_t *dancer);
void d_mad(dancer_t *dancer);
void d_update(dancer_t *dancer);