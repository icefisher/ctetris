# Petyuntris #

Petyuntris is a Tetris implementation on C programming language.
Game is built on SDL library and compiles on both Linux and Windows.

## Windows ##

* Download [petyuntris.zip](https://bitbucket.org/icefisher/ctetris/downloads/petyuntris.zip) game archive.
* Simply unzip and run petyuntris.exe application.

## Linux ##

* Download [petyuntris.tar.gz](https://bitbucket.org/icefisher/ctetris/downloads/petyuntris.tar.gz) game archive.
* Game requires SDL2 library and it extensions: SDL2, SDL2_mixer, SDL2_ttf.
* On Ubuntu you can run these commands:
```
#!bash
$ sudo apt-get install libsdl2-2.0-0 libsdl2-mixer-2.0-0 libsdl2-ttf-2.0-0
$ tar xf petyuntris.tar.gz
$ cd petyuntris
$ ./petyuntris
```

![game.jpg](https://bitbucket.org/repo/qL9p48/images/1638403843-game.jpg)

## Development ##
Windows version can be built using Microsoft Visual C++ 2008 Express Edition or higher.
You need to modify your addition include search directories, linker search directories and libraries names such as SDL2.lib, SDL2_ttf.lib, SDL2_mixer.lib. You can get them here:

* [SDL2](https://www.libsdl.org/download-2.0.php)
* [SDL2_mixer](https://www.libsdl.org/projects/SDL_mixer/)  
* [SDL2_ttf](https://www.libsdl.org/projects/SDL_ttf/)

Linux version can be built using Make, but your need to install development version of SDL2 library and it extensions. For example in Ubuntu your can use command:

```
#!bash
$ sudo apt-get install libsdl2-dev libsdl2-mixer-dev libsdl2-ttf-dev
```

## License ##
Use binary and source code as you wish at your owk risk.

## Credits ##
* Music: Psychedelic - [Bensound.com](http://www.bensound.com/royalty-free-music/track/psychedelic)
* SFX: MadamVicious - [Freesound.com](https://www.freesound.org/people/MadamVicious/sounds/347345/)
* Sprites: Celestialkey - [celestialcoding.com](http://celestialcoding.com/project-advertisment/fruity-dance-clone/)
* Font: Magique Fonts - [dafont.com](http://www.dafont.com/capture-it.font)